__author__ = 'venuktangirala'

import json
import csv

json = json.load(open ("/Users/venuktangirala/yelp_phoenix_academic_dataset/clean/review.json"))

json.iteritems()


with open("/Users/venuktangirala/PycharmProjects/testJson/yelpReview.txt","wb") as yelpReview:
    csv_file =csv.writer(yelpReview, delimiter= '\t')
    for review in json["review"]:

        csv_file.writerow([review["business_id"],
                           review["user_id"],
                           review["type"],
                           review["stars"],
                           review["text"],
                           review["date"],
                           review["votes"]["funny"],
                           review["votes"]["useful"],
                           review["votes"]["cool"]
                           ])
         

yelpReview.close()