import json
import csv

json = json.load(open ("/Users/venuktangirala/yelp_phoenix_academic_dataset/clean/business.json"))
 
json.iteritems()


with open("/Users/venuktangirala/PycharmProjects/testJson/yelpBusiness.txt","wb") as yelpBusiness:
    csv_file =csv.writer(yelpBusiness, delimiter= '\t')
    for business in json["business"]:

        csv_file.writerow([business["business_id"], #0
                           business["categories"],  #1
                           business["city"],        #2
                           business["full_address"],#3
                           business["latitude"],    #4
                           business["longitude"],   #5
                           business["name"],        #6
                           business["neighborhoods"],#7
                           business["open"],
                           business["review_count"],
                           business["stars"],
                           business["state"] ,
                           business["type"]
                            ])
    
yelpBusiness.close()